FactoryGirl.define do
  factory :follower do
    name Faker::Name.name
    twitter_user_id 1
  end
end
