class Tweet < ActiveRecord::Base
  belongs_to :account
  belongs_to :ownerable, polymorphic: true

  scope :latest, -> { order(created_at: :desc) }
  scope :favorites, -> { where(favorite: true).latest }
end
