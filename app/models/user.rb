class User < ActiveRecord::Base
  has_one :account, dependent: :destroy
  has_many :followers, dependent: :destroy
  has_many :followers_tweets, through: :followers
  has_many :tweets, as: :ownerable, dependent: :delete_all

  mount_uploader :image, AvatarUploader
end
