require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module TwitterApp
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true

    config.autoload_paths += %W(#{Rails.root}/lib)
    config.autoload_paths << "#{Rails.root}/app/uploaders"
    config.autoload_paths << "#{Rails.root}/app/services"
  end
end

Rails.application.secrets.each { |key, value| ENV[key.to_s] ||= value }
