class TwitterClient
  def initialize(account)
    @account = account
  end

  def tweet(text)
    client.update(text)
  end

  def tweets(nickname)
    client.user_timeline(nickname)
  end

  def destroy(id)
    client.destroy_status(id)
  end

  def update_name(name)
    client.update_profile(name: name)
  end

  def update_profile_image(file)
    client.update_profile_image(file)
  end

  def follow(id)
    client.follow(id)
  end

  def followers
    client.followers
  end

  def favorites
    client.favorites
  end

  def favorite(id)
    client.favorite(id)
  end

  def unfavorite(id)
    client.unfavorite(id)
  end

  private

  def client
    @client ||= Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_API_KEY']
      config.consumer_secret     = ENV['TWITTER_API_SECRET']
      config.access_token        = @account.token
      config.access_token_secret = @account.secret
    end
  end
end
