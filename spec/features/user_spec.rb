require 'rails_helper'

feature 'User' do
  let(:twitter_client) { TwitterClient }
  let(:account) { create(:account) }
  let(:user) { create(:user, account: account) }
  let(:new_name) { Faker::Name.name }

  before do
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
    visit root_path
  end

  scenario 'Edit', js: true do
    click_link user.nickname
    fill_in 'user_name', with: new_name
    click_button 'Update User'
    expect(page).to have_content(new_name)
  end
end
