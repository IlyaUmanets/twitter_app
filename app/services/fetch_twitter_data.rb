class FetchTwitterData
  def initialize(user, account)
    @user = user
    @account = account
    load_data
  end

  private

  def load_data
    fetch_tweets
    fetch_followers
    fetch_followers_tweets
    fetch_favorites
  end

  def fetch_tweets
    twitter_client.tweets(@user.nickname).each do |tweet|
      @user.tweets.create(tweet_id: tweet.id, text: tweet.text)
    end
  end

  def fetch_followers
    twitter_client.followers.each do |follower|
      @user.followers.create(name: follower.name, twitter_user_id: follower.id)
    end
  end

  def fetch_followers_tweets
    @user.followers.each do |follower|
      twitter_client.tweets(follower.twitter_user_id).each do |tweet|
        follower.followers_tweets.create(tweet_id: tweet.id, text: tweet.text)
      end
    end
  end

  def fetch_favorites
    twitter_client.favorites.each do |tweet|
      @user.tweets.create(tweet_id: tweet.id, text: tweet.text, favorite: true)
    end
  end

  def twitter_client
    @client ||= TwitterClient.new(@account)
  end
end
