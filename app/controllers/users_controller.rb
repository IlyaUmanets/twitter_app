class UsersController < ApplicationController
  before_action :require_user

  def edit
  end

  def update
    current_user.update(user_params)
    update_user_data
    flash.now[:notice] = 'Successfully updated '
  end

  private

  def update_user_data
    twitter_client.update_name(current_user.name) if user_params[:name].present?
    twitter_client.update_profile_image(image_file) if user_params[:image].present?
  end

  def image_file
    File.open(current_user.image.file.file)
  end

  def user_params
    params.require(:user).permit(:name, :image)
  end

  def require_user
    redirect_to root_path unless current_user && current_user.id.to_s == params[:id]
  end

  def twitter_client
    @client ||= TwitterClient.new(current_user.account)
  end
end
