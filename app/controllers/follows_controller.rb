class FollowsController < ApplicationController
  def index
    @followers = current_user.followers
    @tweets = current_user.followers_tweets.latest
  end

  def create
    follower = twitter_client.follow(params[:nickname]).first
    follower = current_user.followers.create(name: follower.name, twitter_user_id: follower.id)
    load_followers_tweets(follower)
    redirect_to follows_path, notice: 'Successfully followed.'
  end

  private

  def load_followers_tweets(follower)
    twitter_client.tweets(follower.twitter_user_id).each do |tweet|
      follower.followers_tweets.create(tweet_id: tweet.id, text: tweet.text)
    end
  end

  def twitter_client
    @client ||= TwitterClient.new(current_user.account)
  end
end
