class SessionsController < ApplicationController
  skip_before_action :require_current_user, only: :create

  def create
    if account
      user
    else
      create_user
      FetchTwitterData.new(user, account)
    end
    session[:user_id] = user.id
    redirect_to tweets_path, notice: 'Welcome!'
  end

  private

  def user
    account.user
  end

  def account
    Account.find_by(provider: request_params[:provider], uid: request_params[:uid])
  end

  def create_user
    User.create(user_attrs).create_account(account_attrs)
  end

  def request_params
    request.env['omniauth.auth']
  end

  def user_attrs
    params_info = request_params[:info]
    {
      nickname: params_info[:nickname],
      name: params_info[:name],
      remote_image_url: params_info[:image]
    }
  end

  def account_attrs
    params_credentials = request_params[:credentials]
    {
      token: params_credentials[:token],
      secret: params_credentials[:secret],
      provider: request_params[:provider],
      uid: request_params[:uid]
    }
  end
end
