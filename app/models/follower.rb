class Follower < ActiveRecord::Base
  has_many :followers_tweets, as: :ownerable, dependent: :delete_all, class_name: 'Tweet'
end
