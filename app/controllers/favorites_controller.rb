class FavoritesController < ApplicationController
  before_action :current_tweet

  def update
    favorite_value = params[:favorite].to_i
    @tweet.update(favorite: favorite_value)
    manage_favorite(favorite_value, @tweet.tweet_id)
    flash[:notice] = flash_message(favorite_value)
  end

  private

  def flash_message(value)
    value.zero? ? 'Successfully unfavorited' : 'Successfully favorited'
  end

  def twitter_client
    @client ||= TwitterClient.new(current_user.account)
  end

  def current_tweet
    @tweet = Tweet.find(params[:id])
  end

  def manage_favorite(value, id)
    value.zero? ? twitter_client.unfavorite(id) : twitter_client.favorite(id)
  end
end
