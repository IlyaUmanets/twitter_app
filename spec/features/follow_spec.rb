require 'rails_helper'

feature 'Follow' do
  let(:twitter_client) { TwitterClient }
  let(:follower) { create(:follower) }
  let(:tweet) { create(:tweet) }
  let(:account) { create(:account) }
  let(:user) { create(:user, account: account) }


  before do
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
    allow_any_instance_of(twitter_client).to receive(:follow) { [follower] }
    allow_any_instance_of(twitter_client).to receive(:tweets) { [tweet] }
    visit root_path
  end

  scenario 'Follow', js: true do
    click_link 'Followers & tweets'
    fill_in 'nickname', with: follower.name
    click_button 'Follow someone'
    expect(page).to have_content(follower.name)
    expect(page).to have_content(tweet.text)
  end
end
