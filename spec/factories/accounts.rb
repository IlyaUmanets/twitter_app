FactoryGirl.define do
  factory :account do
    provider 'twitter'
    uid 1
    token 1
    secret 1
  end
end
