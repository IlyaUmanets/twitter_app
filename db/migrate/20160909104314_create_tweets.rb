class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.belongs_to :account, index: true, foreign_key: true
      t.text :text
      t.string :tweet_id
      t.integer :ownerable_id
      t.string  :ownerable_type
      t.boolean :favorite, default: false

      t.timestamps null: false
    end
  end
end
