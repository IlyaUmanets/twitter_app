require 'rails_helper'

feature 'Favorite' do
  let(:twitter_client) { TwitterClient }
  let(:account) { create(:account) }
  let(:tweet) { create(:tweet) }
  let(:user) { create(:user, account: account, tweets: [tweet]) }

  before do
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
    allow_any_instance_of(twitter_client).to receive(:favorite)
    allow_any_instance_of(twitter_client).to receive(:unfavorite)
    visit root_path
  end

  scenario 'Favorite', js: true do
    click_link('Favorite')
    expect(page).to have_content('Successfully favorited')
    click_link('Unfavorite')
    expect(page).to have_content('Successfully unfavorited')
  end
end
