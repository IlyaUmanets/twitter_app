require 'rails_helper'

feature 'Tweet' do
  let(:twitter_client) { TwitterClient }
  let(:tweet_attrs) { attributes_for(:tweet) }
  let!(:tweet) { create(:tweet) }
  let(:account) { create(:account) }
  let(:user) { create(:user, account: account) }

  before do
    allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
    allow_any_instance_of(twitter_client).to receive(:tweet) { tweet }
    allow_any_instance_of(twitter_client).to receive(:destroy)
    visit root_path
  end

  scenario 'Create / Destroy', js: true do
    fill_in 'tweet_text', with: tweet_attrs[:text]
    click_button 'submit'
    expect(page).to have_content(tweet_attrs[:text])
    click_link 'Delete'
    expect(page).not_to have_content(tweet.text)
  end
end
