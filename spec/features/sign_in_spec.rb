require 'rails_helper'

feature 'Sign in' do
  let(:twitter_client) { TwitterClient }
  let(:tweet) { build(:tweet) }
  let(:follower) { build(:follower) }

  before do
    allow_any_instance_of(twitter_client).to receive(:tweets) { [tweet] }
    allow_any_instance_of(twitter_client).to receive(:followers) { [follower] }
    allow_any_instance_of(twitter_client).to receive(:favorites) { [tweet] }
    stub_omniauth(:twitter)
  end

  scenario 'Sign in / load data', js: true do
    visit root_path
    click_link 'Twitter Signin'
    expect(page).to have_content('test_username')
    expect(page).to have_content(tweet.text)
  end

  def stub_omniauth(provider)
    OmniAuth.config.mock_auth[provider] =
      OmniAuth::AuthHash.new(provider: provider,
                             uid: '12345',
                             nickname: 'test_username',
                             info: { nickname: 'test_username',
                                    name: 'test username' },
                             credentials: { token: :token, secret: :secret })
  end
end
