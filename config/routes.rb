Rails.application.routes.draw do
  root 'tweets#index'

  resources :tweets, only: [:index, :create, :destroy]
  resources :users, only: [:edit, :update]
  resources :follows, only: [:index, :create, :destroy]
  resources :favorites, only: :update
  get '/auth/twitter/callback', to: 'sessions#create'
end
