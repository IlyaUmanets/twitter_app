FactoryGirl.define do
  factory :tweet do
    text Faker::Lorem.sentence
    tweet_id 1
  end
end
