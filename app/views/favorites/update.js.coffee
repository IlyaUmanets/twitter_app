render_body =
  <% if @tweet.ownerable_type == 'Follower' %>
  "<%= j render 'follows/tweet', tweet: @tweet %>"
  <% else %>
  "<%= j render 'tweets/tweet', tweet: @tweet, delete: true %>"
  <% end %>
$(".tweets tr#<%= @tweet.id %>").replaceWith(render_body)
$(".flash_messages").append("<%= j render('shared/flash_messages') %>");
