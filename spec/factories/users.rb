FactoryGirl.define do
  factory :user do
    name Faker::Name.name
    nickname Faker::Internet.user_name
  end
end
