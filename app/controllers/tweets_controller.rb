class TweetsController < ApplicationController
  skip_before_action :require_current_user, only: :index

  def index
    @tweet = Tweet.new
  end

  def create
    new_tweet = twitter_client.tweet(tweet_params[:text])
    @tweet = current_user.tweets.create(tweet_params.merge(tweet_id: new_tweet.id))
    flash.now[:notice] = 'Tweet was successfully created.'
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    twitter_client.destroy(@tweet.tweet_id)
    @tweet.destroy
    flash.now[:notice] = 'Tweet was successfully deleted.'
  end

  private

  def tweet_params
    params.require(:tweet).permit(:text)
  end

  def twitter_client
    TwitterClient.new(current_user.account)
  end

  def own_tweets
    current_user.tweets.latest
  end
  helper_method :own_tweets

  def favorites_tweets
    current_user.tweets.favorites.latest
  end
  helper_method :favorites_tweets
end
